
//--------------------------------------------МЕТОД ПРОСТЫХ ИТЕРАЦИЙ----------------------------------------------------

//метод простых итераций, решает СЛАУ с заданной точностью eps, возвращает массив со сзначением неизвестных
function simpleIterationMethod (matrix, eps){
    
    var previousVariableValues = [], size = matrix.length, error, currentVariableValues;
    for (var k = 0; k < size; k++) previousVariableValues[k] = 0.0;
    
    while (true) {
        currentVariableValues = [];
        for(var i = 0; i < size; i++){
            currentVariableValues[i] = matrix[i][size];
            for (var j = 0; j < size; j++){
                if(i != j) currentVariableValues[i] -= matrix[i][j]*previousVariableValues[j];
            }
            currentVariableValues[i] /= matrix[i][i];
        }
        
        error = 0.0;
        for (var z = 0; z < size; z++) error += Math.abs(currentVariableValues[z] - previousVariableValues[z]);
        if(error < eps) break;
        if(error > 100){
            alert("Достигнута превышающая норму погрешность решения. " +
                "Ответ не корректный. Пожалуйста, приведите с-му к диагональному преобладанию и повторите попытку");
            break;
        }
        previousVariableValues = currentVariableValues;
    }
    return previousVariableValues;
}

//проверка на диагональное преобладание
function isDiagonalPrevalenced(matrix){
    matrix = formMatrix(matrix, 0);
    for (var i = 0; i < matrix.length; i++){
        var sum = Math.abs(countSum(matrix[i], i));
        if (!(Math.abs(matrix[i][i]) > sum)) {
	    alert('False!');
		return false;
		}
    }
    return true;
}

//подсчет суммы коэфициентов в векторе уравнения
function countSum(vector, index) {
    var sum = 0;
    for (var i = 0; i < vector.length; i++){
        if (i!=index) sum += vector[i];
    }
    return sum;
}


//----------------------------------------ВСПОМОГАТЕЛЬНЫЕ ФУНКЦИИ-------------------------------------------------------

//демонстрационная часть и точка запуска программы
//Обработка нажатия кнопки - собственно считывание массива, и вызов всех функций для него.

function buttonGo(Obj) {
	var count = +document.getElementById('count').value;
	var rezArr = [];
	for(var i = 0; i < count; i++){
	var constArr = [];
	    for(var j = 0; j<count; j++){
			var id = i+'_'+j;
			constArr[j]=+document.getElementById(id).value;
		}
		var id = i+'_'+count;
		constArr[count]=+document.getElementById(id).value;
		rezArr[i]=constArr;
	}
	
	if(isDiagonalPrevalenced(rezArr)){
        alert("СЛАУ удовлетворяет условия диагонального преобладания");
        var iter = simpleIterationMethod(rezArr, 0.00001);
		print_rezult(iter,"iter","Метод простых итераций:");
    }else{
        if(confirm("СЛАУ НЕ удовлетворяет условия диагонального преобладания. Возможно наличие ошибки. Желаете продолжить?"))
        var iter = simpleIterationMethod(rezArr, 0.00001);
		print_rezult(iter,"iter","Метод простых итераций:");
    }
	calcRootsKramer(rezArr);
}


//Вывод заголовка name, затем вывод массива в таблицу, и вставка их в элемент с именем id.
function print_rezult(arr,id,name) {
    //Создаем строку и пишем в нее весь текст+html теги.
    //Получается таблица.
    var html = "<h1>"+name+"</h1>";
	html+='<table>';
    html+='<tr>';
    for (var i=0; i<arr.length;i++){
        html+='<th>'+'x_'+(i+1)+'</th>';
    }
    html+='</tr>';
	html+='<tr>';
    for (var i=0; i<arr.length;i++){
        html+='<td>'+arr[i]+'</td>';
    }
	html+='</tr>';
    html+='</table>';
    //Находим в файле элемент с индексом id и записываем в него нашу строку
    document.getElementById(id).innerHTML=html;
}

 
 

//--------------------------------------------МЕТОД КРАМЕРА-------------------------------------------------------------

/*
 Напомню алгоритм для метода Крамера:
 1) Считаем главный определитель матрицы D
 Если он не равен нулю, то имеем единственное решение , которое нам и нужно найти
 2) Считаем вспомагательные определители Dі, заменяя і-тый столбец значениями столбца свободных членов
 3)Нахадим корни: Хі=Dі/D
 */

/*Подсчёт определителя*/
function compDeterminant (matrix) {
//J  - порядок матрицы
    var J = matrix.length; if (J == 2)//простейший подсчёт для матрицы 2х2
        return (matrix [0] [0] * matrix [1] [1] - matrix [0] [1] * matrix [1] [0]);
    /*для матрицы большей размерности:
     Используем теорему Лапласа(Если в определителе виделить к строк/столбцов, то определитель равен сумме произведений
     всяких миноров к-ого порядка,которые нахлдятся в этих строках/столбцах на их алгебраическое дополнение)
     +минор к-ого порядка опеределителя det - это определитель к-ого пор, составленный из эл. определителя det, которые находяться на пересечении
     выделеных к строк и к столбцов определителя det
     +алг.дополнение -это дополняющий минор (определитель составленный из эл, что остались, после вычёркивания строк и столбцов в которых находится минор)
     взятый со знаком (-1)^s, s -сумма номеров строк и столбцов, в которых находится минор
     */

    for (var tmp = [], det = j = 0; j < J; j++)
    {
        var minor = matrix.slice (1); //вычеркнули первую строчку и первый столбец
        for (var k = 1; k < J; k++)
            minor [k - 1] = matrix [k].slice (0, j).concat (matrix [k].slice (j + 1));
        //Math.pow (-1, j) * matrix [0] [j]= алг.дополнение*минор
        det += Math.pow (-1, j) * matrix [0] [j] * compDeterminant (minor);

    }
    return det;
}

/*функция для разбиения матрицы на миноры*/
function formMatrix (rezArr, type) {
    for (var matrix = [], j = 0, J = rezArr.length; j < J; j++)
    {
        matrix [j] = rezArr [j].slice (0, rezArr [j].length - 1);
        if (type) matrix [j] [type - 1] = rezArr [j] [rezArr [j].length - 1];//замена на столбец свободных членов
    }
    return matrix;
}

/*Вычисление корней*/
function calcRootsKramer (rezArr) {

    //document.write("<br />" + "МЕТОД КРАМЕРА:" + "<br />");
    var d0 = compDeterminant (formMatrix (rezArr, 0));//Подсчёт главного определителя
//если определитель равен нулю
    if (d0==0) {
        if (isCompatible(rezArr)==1)  alert("Система имеет бесконечно много решений. Метод Крамера отказывается работать.");
        if (isCompatible(rezArr)==0)  alert("Система несовместима(не имеет решений). Метод Крамера отказывается работать.");
    }
    else{
        // roots - массив искомых корней
        for (var roots = [], j = 0, J = rezArr.length; j < J; j++)
        {roots [j] = compDeterminant (formMatrix (rezArr, j + 1)) / d0;}
			print_rezult(roots,"kra","Метод Крамера:");
		}
}

/*
 Для оперделения совместима ли система используем теорему Кронекера-Капелли
 (Для того, чтобы СЛАР была совместной необходимо и достаточно, чтобы ранг матрицы системы был равен рангу рсширенной матрицы)

 +ранг матрицы - это наибольший порядок минора , отличного от нуля .Тоесть: матрица А миеет ранг r, если среди её миноров существует хотябы
 один минор порядка r отличный от нуля , а все миноры порядка (r+1) и выше равны нулю или не существуют
 */

//Определяет совместима ли система
function isCompatible(matrix){

    var rankMatrix=rangMat(formMatrix(matrix,0)),rankAugmentedMatrix = rangMat(matrix),compatible;
    if (rankMatrix==rankAugmentedMatrix) compatible = 1;
    else compatible = 0;
    return compatible;
}

//Подсчёт ранга матрицы
function rangMat(matrix) {
    /*Алгоритм нахождения ранга матрицы с помощью миноров */
    var r = 0, q = 1, m =matrix.length, n = matrix[0].length,i, a,b, c, d, B = [];
    while(q<=min(m,n)) // проверка: порядок матрицы меньше или равен минимальному из размеров матрицы?
    { // если да
        for ( i = 0; i < q; i++) B[i] = [];
        for(a=0;a<(m-(q-1));a++) // тут начинается перебор матриц q-го порядка
        {
            for( b=0;b<(n-(q-1));b++)
            {
                for( c=0;c<q;c++)
                {
                    for( d=0;d<q;d++)
                    {
                        B[c][d] = matrix[a+c][b+d];
                    }
                }
                if(!(compDeterminant(B)==0)) // если определитель матрицы отличен от нуля
                { // то
                    r = q; // присваиваем рангу значение q
                }
            }
        }
        q++; // прибавляем 1
    }
    return r;

}

//Поиск меньшего числа
function min(a,b){
    return(a >= b)? b : a;
}

/*Тестирование определителя. Выводит основной определитель системы уравнений.*/
function CheckDet(A) {
    var N = A.length, B = [];
    for (var i = 0; i < N; ++i){
        B[i] = [];
        for (var j = 0; j < A[i].length-1; ++j) {B[i][j] = A[i][j];}
    }
    printDetM(B);
    print("Определитель: " + compDeterminant(B)+"<br />" );
}
//----------------------------------------------------------------------------------------------------------------------




